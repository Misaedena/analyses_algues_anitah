library(readxl)
library(gtsummary)
library(tidyverse)
library(labelled)
library(extrafont)


df <- read_excel('tsaramaso_algue.xlsx',sheet = 'tsaramaso_algue')

names(df)[c(9:13)]<-c('Feuille_manioc','Amer','Fruit_de_mer','Spiruline','Inconnu')

df$Feuille_manioc <- ifelse(df$Feuille_manioc == 1,'Oui','Non')
df$Amer <- ifelse(df$Amer == 1,'Oui','Non')
df$Fruit_de_mer <- ifelse(df$Fruit_de_mer == 1,'Oui','Non')
df$Spiruline <- ifelse(df$Spiruline == 1,'Oui','Non')
df$Inconnu <- ifelse(df$Inconnu == 1,'Oui','Non')

df %>% 
  tbl_summary(
    include = c('Feuille_manioc','Amer','Fruit_de_mer','Spiruline','Inconnu'),
    by = 'produit'
  ) %>% add_p()

df1$Saveur <- recode_factor(df1$Saveur,
                            'Feuille_manioc' = 'Feuille de Manioc',
                            'ruit_de_mer' = 'Fruit de mer',
                            'Amère' = 'Amer')

df1 <- read_excel('Histogramme.xlsx',sheet = 'saveur')
ggplot(df1,aes(x = Saveur,fill = présence,y = Valeur))+
  geom_bar(stat = 'identity',position = 'fill')+
  scale_fill_manual(values = c('gray','black'),
                    name = '')+
  scale_y_continuous(labels = c('0%','25%','50%','75%','100%'))+
  theme_clean()+
  theme(legend.title = element_blank(),
        axis.title = element_blank(),
        axis.text = element_text(family = 'Times New Roman'),
        legend.text = element_text(family = 'Times New Roman'),
        legend.direction = 'horizontal',
        legend.position = 'bottom',
        axis.text.x = element_text(angle = 45,hjust = 1))+
  facet_grid(.~ Produit)

ggsave(filename = 'Saveur.png',width = 9,height = 6,dpi = 1000)












